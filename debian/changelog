jruby-joni (2.1.43-2) unstable; urgency=medium

  * debian/control
    - Set Standards-Version: 4.6.1 with no changes
    - Remove Torsten Werner <twerner@debian.org> from Uploaders

 -- Hideki Yamane <henrich@debian.org>  Sun, 29 May 2022 17:17:00 +0900

jruby-joni (2.1.43-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Mon, 11 Apr 2022 20:26:29 +0900

jruby-joni (2.1.42-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Fri, 25 Mar 2022 22:39:19 +0900

jruby-joni (2.1.41-2) unstable; urgency=medium

  * debian/control
    - Set Standards-Version: 4.6.0 with no change
  * debian/watch
    - Fix watch URL
  * Drop unused debian/lintian-overrides

 -- Hideki Yamane <henrich@debian.org>  Fri, 29 Oct 2021 23:19:53 +0900

jruby-joni (2.1.41-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - set Standards-Version: 4.5.1
    - Adjust Build-Depends: junit version to 4.13.1
  * Add debian/lintian-overrides

 -- Hideki Yamane <henrich@debian.org>  Sat, 30 Jan 2021 22:05:11 +0900

jruby-joni (2.1.40-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - Adjust Build-Depends:

 -- Hideki Yamane <henrich@debian.org>  Fri, 05 Jun 2020 09:00:51 +0900

jruby-joni (2.1.39-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - Set Build-Depends-Indep: libjcodings-java (>= 1.0.54)

 -- Hideki Yamane <henrich@debian.org>  Sun, 24 May 2020 00:37:50 +0900

jruby-joni (2.1.37-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - Bump to "Build-Depends-Indep: libjcodings-java (>= 1.0.53)"

 -- Hideki Yamane <henrich@debian.org>  Mon, 18 May 2020 08:31:56 +0900

jruby-joni (2.1.36-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - Bump up Build-Depends-Indep: libjcodings-java (>= 1.0.51)
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Hideki Yamane <henrich@debian.org>  Tue, 12 May 2020 23:57:03 +0900

jruby-joni (2.1.34-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - Bump up Build-Depends-Indep: libjcodings-java (>= 1.0.50)
  * debian/salsa-ci.yml
    - set "reprotest allow_failure: true"

 -- Hideki Yamane <henrich@debian.org>  Sat, 02 May 2020 19:24:24 +0900

jruby-joni (2.1.33-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - Bump up to debhelper-compat (= 13)
    - Bump up to Build-Depends-Indep: libjcodings-java (>= 1.0.49)

 -- Hideki Yamane <henrich@debian.org>  Mon, 27 Apr 2020 19:22:41 +0900

jruby-joni (2.1.31-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - update Build-Depends-Indeps: libjcodings-java (>= 1.0.46)
    - set Standards-Version: 4.5.0

 -- Hideki Yamane <henrich@debian.org>  Mon, 24 Feb 2020 22:50:58 +0900

jruby-joni (2.1.30-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - adjust dependency version to libjcodings-java

 -- Hideki Yamane <henrich@debian.org>  Sat, 02 Nov 2019 00:58:55 +0900

jruby-joni (2.1.29-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - adjust dependency to jcodings 1.0.44
    - set Standards-Version: 4.4.0
  * add debian/salsa-ci.yml to exec CI on salsa.debian.org
  * debian/gbp.conf
    - drop pbuilder to avoid CI failure on salsa

 -- Hideki Yamane <henrich@debian.org>  Mon, 12 Aug 2019 12:54:28 +0900

jruby-joni (2.1.28-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Thu, 23 May 2019 17:41:47 +0900

jruby-joni (2.1.27-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - drop libasm-java dependency
    - set Rules-Requires-Root: no

 -- Hideki Yamane <henrich@debian.org>  Sat, 18 May 2019 11:12:28 +0900

jruby-joni (2.1.26-2) unstable; urgency=medium

  * debian/control
    - adjust Build-Depends to specify jcodings (>= 1.0.43) as pom.xml says

 -- Hideki Yamane <henrich@debian.org>  Sat, 09 Mar 2019 19:12:34 +0900

jruby-joni (2.1.26-1) unstable; urgency=medium

  * New upstream release
    - fix tests fail with invalid character property name
      <graphemeclusterbreak=emodifier> (Closes: #923295)
  * debian/control
    - set Standards-Version: 4.3.0
    - use dh12
  * debian/compat
    - drop it

 -- Hideki Yamane <henrich@debian.org>  Thu, 07 Mar 2019 16:59:41 +0900

jruby-joni (2.1.25-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - Adjust Build-Dependency version

 -- Hideki Yamane <henrich@debian.org>  Sat, 20 Oct 2018 07:46:51 +0900

jruby-joni (2.1.24-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Tue, 09 Oct 2018 02:59:49 +0900

jruby-joni (2.1.23-1) unstable; urgency=medium

  * New upstream release
  * debian/copyright
    - use https for upstream contact and source URL

 -- Hideki Yamane <henrich@debian.org>  Wed, 03 Oct 2018 06:36:47 +0900

jruby-joni (2.1.21-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - Adjust Build-Dependency version
    - set Standards-Version: 4.2.1 with no change
  * debian/patches
    - drop build.xml-jcoding.patch due to drop build.xml in upstream

 -- Hideki Yamane <henrich@debian.org>  Tue, 02 Oct 2018 19:50:49 +0900

jruby-joni (2.1.18-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - set Standards-Version: 4.2.0 with no change

  [ Thomas E. Enebo ]
  * [maven-release-plugin] prepare for next development iteration

  [ Marcin Mielzynski ]
  * fix escape warning
  * fix char class warning
  * bump java since jcodings is already on 7
  * remove unused settings
  * move nameTable logic to scan environment

  [ lopex ]
  * don't use deprecated apis
  * Fixes #35

  [ Marcin Mielzynski ]
  * bump jcodings
  * move name table back to regex
  * ability to pass null region
  * remove USE_POSIX_API_REGION_OPTION

  [ Marcin Mielzynski ]
  * add matcherNoRegion byte[] version for consistency

  [ Charles Oliver Nutter ]
  * Provide a lighter-weight mechanism for interrupting.

 -- Hideki Yamane <henrich@debian.org>  Mon, 13 Aug 2018 17:07:26 +0900

jruby-joni (2.1.16-2) unstable; urgency=medium

  * debian/rules
    - Remove unnecessary get-orig-source target
  * debian/control
    - Update Vcs-* to use salsa.debian.org
    - Set Standards-Version: 4.1.4
  * debian/copyright
    - Update copyright year

 -- Hideki Yamane <henrich@debian.org>  Fri, 01 Jun 2018 09:57:21 +0900

jruby-joni (2.1.16-1) unstable; urgency=medium

  * New upstream release

  [ Thomas E. Enebo ]
  * [maven-release-plugin] prepare for next development iteration
  * Update to new jcodings 1.0.28
  * [maven-release-plugin] prepare release joni-2.1.16

  [ Marcin Mielzynski ]
  * move debug info
  * remove redundant name prefix

  [ lopex ]
  * whoops, make those static

  [ Hideki Yamane ]
  * debian/control
    - adjust dependency to jcodings 1.0.28
    - set Build-Depdnds: debhelper (>= 11)
  * debian/compat
    - set 11

 -- Hideki Yamane <henrich@debian.org>  Wed, 14 Mar 2018 22:47:32 +0900

jruby-joni (2.1.15-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Tue, 13 Mar 2018 20:21:12 +0900

jruby-joni (2.1.14-1) unstable; urgency=medium

  * New upstream release
  * debian/patches
    - drop 0002-remove-build-time-for-reproducible-build.patch: merged upstream
  * debian/control
    - set Standards-Version: 4.1.3
    - update Dependency version
  * debian/maven.rules
    - update junit version to 4.x to avoid FTBFS

 -- Hideki Yamane <henrich@debian.org>  Sat, 10 Feb 2018 21:16:27 +0900

jruby-joni (2.1.13-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - set Standards-Version: 4.1.2

 -- Hideki Yamane <henrich@debian.org>  Mon, 11 Dec 2017 21:32:03 +0900

jruby-joni (2.1.12-2) unstable; urgency=medium

  * debian/control
    - set Standards-Version: 4.1.1
    - set Priority: optional

 -- Hideki Yamane <henrich@debian.org>  Wed, 25 Oct 2017 20:49:39 +0900

jruby-joni (2.1.12-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - set Standards-Version: 4.0.1 with no change
  * debian/copyright
    - update copyright information and upstream contact as JRuby team

 -- Hideki Yamane <henrich@debian.org>  Wed, 16 Aug 2017 21:19:31 +0900

jruby-joni (2.1.11-3) unstable; urgency=medium

  * debian/control
    - set Standards-Version: 4.0.0
    - set Build-Depends: debhelper (>= 10)
    - add Multi-Arch: foreign
  * debian/compat
    - set 10
  * debian/watch
    - update to version4
  * debian/patches
    - add 0003-add-class-path-for-jcodings.patch to eliminate lintian warning
      "missing-classpath libjcodings-java"

 -- Hideki Yamane <henrich@debian.org>  Wed, 19 Jul 2017 21:30:24 +0900

jruby-joni (2.1.11-2) unstable; urgency=medium

  * Team upload.
  * Build with Maven instead of Ant and enabled the unit tests
  * Depend on libasm-java (>= 5.0) instead of libasm4-java
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 03 Oct 2016 22:35:08 +0200

jruby-joni (2.1.11-1) unstable; urgency=medium

  * New upstream release
  * debian/gbp.conf
    - set "pbuilder = True"
  * debian/control
    - set Standards-Version: 3.9.8

 -- Hideki Yamane <henrich@debian.org>  Sun, 21 Aug 2016 15:23:16 +0900

jruby-joni (2.1.10-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - set Standards-Version: 3.9.7
    - use https for Homepage and Vcs-*

 -- Hideki Yamane <henrich@debian.org>  Sat, 19 Mar 2016 08:22:03 +0900

jruby-joni (2.1.9-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Fri, 25 Dec 2015 22:42:32 +0900

jruby-joni (2.1.8-3) unstable; urgency=medium

  * debian/copyright
    - fix "dep5-copyright-license-name-not-unique" warning

 -- Hideki Yamane <henrich@debian.org>  Mon, 14 Dec 2015 06:51:27 +0900

jruby-joni (2.1.8-2) unstable; urgency=medium

  * debian/control
    - adjust libjcodings-java version to >= 1.0.13
    - use libasm4-java, instead of 3 (Closes: #800852)
  * debian/patches
    - update build.xml-jcoding.patch to use asm4

 -- Hideki Yamane <henrich@debian.org>  Tue, 17 Nov 2015 06:03:58 +0900

jruby-joni (2.1.8-1) unstable; urgency=medium

  * New upstream release
  * add debian/gbp.conf for git-buildpackage

 -- Hideki Yamane <henrich@debian.org>  Mon, 16 Nov 2015 20:19:15 +0900

jruby-joni (2.1.7-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Tue, 20 Oct 2015 07:03:39 +0900

jruby-joni (2.1.6-2) unstable; urgency=medium

  * debian/patches
    - add 0002-remove-build-time-for-reproducible-build.patch for reproducible
      build

 -- Hideki Yamane <henrich@debian.org>  Sat, 06 Jun 2015 13:07:46 +0900

jruby-joni (2.1.6-1) unstable; urgency=medium

  * New Upstream version 2.1.6

 -- Hideki Yamane <henrich@debian.org>  Wed, 29 Apr 2015 23:22:59 +0900

jruby-joni (2.1.5-1) unstable; urgency=medium

  * New Upstream version 2.1.5
  * debian/rules
    - use override_dh_auto_build target to apply debian/ant.properties value
      properly. It fixes lintian warning "incompatible-java-bytecode-format".
  * debian/control
    - set Standards-Version: 3.9.6

 -- Hideki Yamane <henrich@debian.org>  Tue, 30 Dec 2014 15:06:52 +0900

jruby-joni (2.1.4-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sat, 25 Oct 2014 21:17:42 +0900

jruby-joni (2.1.2-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sat, 13 Sep 2014 00:16:58 +0900

jruby-joni (2.1.0-2) unstable; urgency=low

  * Team upload.
  * Provide Maven artifacts. (Closes: #742505).

 -- Miguel Landaeta <nomadium@debian.org>  Mon, 24 Mar 2014 12:00:16 -0300

jruby-joni (2.1.0-1) unstable; urgency=medium

  * New upstream release
  * debian/watch
    - deal with upstream's naming scheme change

 -- Hideki Yamane <henrich@debian.org>  Sun, 16 Feb 2014 17:14:36 +0900

jruby-joni (2.0.0-1) unstable; urgency=low

  * New upstream release
  * remove unnecessary debian/libjruby-joni-java.lintian-overrides file

 -- Hideki Yamane <henrich@debian.org>  Tue, 04 Jun 2013 19:32:05 +0900

jruby-joni (1.1.9-3) unstable; urgency=low

  * Upload to unstable

 -- Hideki Yamane <henrich@debian.org>  Tue, 07 May 2013 13:27:34 +0900

jruby-joni (1.1.9-2) experimental; urgency=low

  * Team upload.
  * debian/rules
    - use debian/ant.properties to specify version to avoid incompatible
      java bytecode format with OpenJDK 7 issue (LP: #1049784)
  * add debian/ant.properties
    - use "ant.build.javac.{source,target}=1.6"
  * debian/control
    - set "Standards-Version: 3.9.4"

 -- Hideki Yamane <henrich@debian.org>  Sat, 15 Dec 2012 08:26:06 +0900

jruby-joni (1.1.9-1) experimental; urgency=low

  * New upstream release
  * debian/watch
    - fixed to work, thanks to Bart Martens <bartm@debian.org>

 -- Hideki Yamane <henrich@debian.org>  Tue, 27 Nov 2012 22:18:32 +0900

jruby-joni (1.1.7+git20120721-1) experimental; urgency=low

  * Team upload.
  * New upstream release
  * debian/control
    - update my email address
    - set "Standards-Version: 3.9.3"
    - update "Depends: libjcodings-java (>= 1.0.9-1)" since newer jcodings
      version is required
  * debian/watch
    - watch "tags", not master branch
  * debian/orig-tar.sh
    - put file to appropriate path
  * debian/copyright
    - fix and update to copyright-format 1.0
  * debian/patches
    - update build.xml-jcoding.patch file to fix lintian warning
      "missing-classpath libjcodings-java, libasm3-java"
  * debian/libjruby-joni-java.lintian-overrides
    - add it to ignore warning, jruby libraries are put into under
      /usr/lib/jruby/lib/

 -- Hideki Yamane <henrich@debian.org>  Sat, 21 Jul 2012 08:54:04 +0900

jruby-joni (1.1.4-2) unstable; urgency=low

  * Team upload.
  * d/control: Add default-jdk to Build-Depends (Closes: #593017)
  * d/control: Add Depends on libjcodings-java, libasm3-java

 -- Damien Raude-Morvan <drazzib@debian.org>  Sat, 09 Oct 2010 12:37:01 +0200

jruby-joni (1.1.4-1) unstable; urgency=low

  * New upstream release
  * Add myself to Uploaders.
  * Remove Build-Depends: quilt and the file debian/README.source.
  * Update Build-Depends: libjcodings-java.
  * Fix debian/watch and add get-orig-source target.
  * Push package to git and add Vcs headers.
  * Set Maintainer to Debian Java Maintainers.
  * Improve long description.
  * Correct the copyright file.

 -- Torsten Werner <twerner@debian.org>  Sun, 01 Aug 2010 22:16:34 +0200

jruby-joni (1.1.3-1) unstable; urgency=low

  * Initial release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Fri, 19 Mar 2010 10:32:00 +0900
